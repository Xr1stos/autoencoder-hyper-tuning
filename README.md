# Autoencoder hyper tuning


An Autoencoder hyperparameter tuning notebook using hyperas (v. 0.4.1).

GPU enabled.



# Prerequisites: 

**Python 3 with:**  

hyperas  
hyperopt  
keras  
numpy  
pandas  
sklearn  
tensorflow  


